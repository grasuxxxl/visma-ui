export function authenticate(credentials) {
  return fetch("api/authenticate", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(credentials)
  }).then(response => response.json());
}

export function forgot({ email }) {
  return fetch("api/forgot", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ email })
  }).then(response => response.json());
}

export function recover({ token, password }) {
  return fetch("api/recover", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ token, password })
  }).then(response => response.json());
}
