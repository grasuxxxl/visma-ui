

export function search({ query, maxId }) {
  const encodedQuery = encodeURIComponent(query);
  const encodedMaxId = encodeURIComponent(maxId);

  return fetch(`api/tweets/search?query=${encodedQuery}&maxId=${encodedMaxId}`, {
    method: "GET"
  })
    .then(response => response.json());
}