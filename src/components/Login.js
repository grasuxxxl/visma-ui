import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Input from '../controls/Input';
import { authenticate } from '../services/authentication';
import './Login.css';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {username: '', password: ''};

    this.onUsernameChange = this.onUsernameChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
  }

  onLogin() {
    const { onLoginSuccess = () => {} } = this.props;

    authenticate(this.state)
      .then(user => {
        onLoginSuccess({ user });
      });
  }

  onUsernameChange(event) {
    const value = event.target.value;
    this.setState({ username: value });
  }
  onPasswordChange(event) {
    const value = event.target.value;
    this.setState({ password: value });
  }

  render() {
    return (
      <div className="materialContainer">
        <div className="box">
          <div className="title">LOGIN</div>
          <Input onChange={this.onUsernameChange} name="username" label="Username"/>
          <Input onChange={this.onPasswordChange} name="password" type="password" label="Password"/>

          <div className="button login">
            <button onClick={this.onLogin.bind(this)}><span>GO</span><i className="fa fa-check"></i></button>
          </div>

          <Link to="/forgot-password">
            <span className="pass-forgot">Forgot your password?</span>
          </Link>
        </div>
      </div>
    );
  }
}