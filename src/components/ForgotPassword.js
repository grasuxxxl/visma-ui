import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Input from '../controls/Input';
import { forgot } from '../services/authentication';
import './Login.css';

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = { email: '', recoverState: 'NOT_STARTED' }; // NOT_STARTED | STARTED | SUCCESS

    this.onEmailChange = this.onEmailChange.bind(this);
  }

  onRecover() {
    forgot(this.state)
      .then(() => {
        this.setState({ recoverState: "SUCCESS" });
      });
  }

  onOk () {
    const { history } = this.props;
    history.push("/");
  }

  onEmailChange(event) {
    const value = event.target.value;
    this.setState({ email: value });
  }

  render() {
    const shouldShowInput = this.state.recoverState === "NOT_STARTED";

    const content = shouldShowInput
      ? [
      <div className="title">FORGOT PASSWORD</div>,
      <Input onChange={this.onEmailChange} name="email" label="Email"/>,

      <div className="button login">
        <button onClick={this.onRecover.bind(this)}><span>Recover</span><i className="fa fa-check"></i></button>
      </div>
    ]
      : [
      <div className="title">FORGOT PASSWORD</div>,
      <div>An email has been sent with instructions regarding password recovery</div>,
      <div className="button login">
        <button onClick={this.onOk.bind(this)}><span>OK</span><i className="fa fa-check"></i></button>
      </div>
    ]

    return (
      <div className="materialContainer">
        <div className="box">
          { content }
        </div>
      </div>
    );
  }
}


export default withRouter(ForgotPassword);