import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import Input from "../controls/Input";
import { recover } from "../services/authentication";
import "./Login.css";

class RecoverPassword extends Component {
  constructor(props) {
    super(props);
    this.state = { password: "" };

    this.onPasswordChange = this.onPasswordChange.bind(this);
  }

  onRecover() {
    const search = window.location.search; // could be '?foo=bar'
    const params = new URLSearchParams(search);
    const token = params.get("token");
    const { password } = this.state;

    recover({ token, password }).then(() => {
      const { history } = this.props;
      history.push("/");
    });
  }

  onPasswordChange(event) {
    const value = event.target.value;
    this.setState({ password: value });
  }

  render() {
    return (
      <div className="materialContainer">
        <div className="box">
          <div className="title">RECOVER PASSWORD</div>,
          <Input
            onChange={this.onPasswordChange}
            type="password"
            name="password"
            label="Password"
          />
          <div className="button login">
            <button onClick={this.onRecover.bind(this)}>
              <span>Recover</span>
              <i className="fa fa-check" />
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(RecoverPassword);
