import React, { Component } from 'react';
import PropTypes from 'prop-types';


export default class NotAuthenticated extends Component {
  static contextTypes = {
    user: PropTypes.object
  };

  render () {
    const user = this.context.user;
    const isAuthenticated = user !== null;

    return isAuthenticated ? null : this.props.children;
  }
}