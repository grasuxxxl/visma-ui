import React, { Component } from 'react';
import ReactTweet from 'react-tweet';
import {
  List, WindowScroller, InfiniteLoader,
  CellMeasurer, CellMeasurerCache, AutoSizer } from 'react-virtualized';
import { search } from '../services/tweets';
import './VismaTwitter.css';


function Tweet({cache, tweets}, { index, key, parent, style }) {
  const tweet = tweets[index];
  const linkProps = {target: '_blank', rel: 'noreferrer' };

  const content = tweet === undefined
    ? (<div>Loading</div>)
    : (
    <div style={style}>
      <ReactTweet data={tweet} linkProps={linkProps} />
    </div>
  );


  return (
  <CellMeasurer
    key={key}
    cache={cache}
    columnIndex={0}
    parent={parent}
    rowIndex={index}>
    {content}
  </CellMeasurer>

  );
}

export default class VismaTwitter extends Component {
  constructor() {
    super();
    this.state = { tweets: null, isLoading: true };
    this.cache = new CellMeasurerCache({
      defaultHeight: 100,
      fixedWidth: true
    });
  }

  _isRowLoaded({index}) {
    const { tweets } = this.state;
    return !!tweets[index];
  }

  loadTweets() {
    this.setState({ isLoading: true }, () => {
      search({query: "a"})
        .then(({ statuses, search_metadata }) => {
          this.setState({ tweets: statuses, search_metadata, isLoading: false });
        })
    });

  }

  loadNextTweets() {
    const maxId = new URLSearchParams(this.state.search_metadata.next_results).get("max_id");

    this.setState({ isLoading: true }, () => {
      search({query: "a", maxId })
        .then(({ statuses, search_metadata }) => {
          const { tweets } = this.state;
          this.setState({ tweets: [...tweets, ...statuses], search_metadata, isLoading: false });
        });
    });
  }

  hasMoreTweets() {
    const { search_metadata = {} } = this.state;
    const { next_results } = search_metadata;
    return next_results !== undefined || next_results !== null;
  }

  componentDidMount() {
    this.loadTweets();
  }

  render() {
    const { tweets, isLoading } = this.state;
    if (!tweets && isLoading) return <div>"Loading"</div>;

    const loadMoreRows = isLoading ? () => {} : this.loadNextTweets.bind(this);
    const totalTweets = this.hasMoreTweets() ? tweets.length + 5 : tweets.length;
    const cache = this.cache;

    return (
      <InfiniteLoader
        isRowLoaded={this._isRowLoaded.bind(this)}
        loadMoreRows={loadMoreRows}
        rowCount={totalTweets}
        threshold={40}>
        {({onRowsRendered, registerChild}) => (
          <WindowScroller>
            {({ height, isScrolling, onChildScroll, scrollTop }) => (
              <AutoSizer disableHeight>
                {({ width }) => (
                  <List
                    className="UsersInfiniteWindowList"
                    ref={registerChild}
                    autoHeight
                    height={height}
                    onRowsRendered={onRowsRendered}
                    isScrolling={isScrolling}
                    onScroll={onChildScroll}
                    scrollTop={scrollTop}
                    rowCount={tweets.length}
                    // rowHeight={100}
                    rowHeight={cache.rowHeight}
                    deferredMeasurementCache={cache}
                    width={width}
                    rowRenderer={Tweet.bind(null, { cache, tweets })}>
                  </List>
                )}
              </AutoSizer>
            )}
          </WindowScroller>
        )}
      </InfiniteLoader>
    )
  }
}