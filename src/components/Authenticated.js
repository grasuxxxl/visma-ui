import React, { Component } from 'react';
import PropTypes from 'prop-types';


export default class Authenticated extends Component {
  static contextTypes = {
    user: PropTypes.object
  };

  render () {
    const user = this.context.user;
    const isAuthenticated = user !== null;

    return isAuthenticated ? this.props.children : null;
  }
}