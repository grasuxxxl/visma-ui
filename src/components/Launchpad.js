import React, { Component } from "react";
import { Link } from "react-router-dom";
import twitter from "../resources/twitter.svg";
import "./Launchpad.css";

export default class Launchpad extends Component {
  render() {
    return (
      <div>
        <div className="Header">
          <h1>Visma Launchpad</h1>
        </div>
        <div className="TileContainer">
          <Link to="/visma-twitter" className="Tile">
            <div className="Tile-Title">#Visma</div>
            <img className="Tile-Icon" src={twitter} />
          </Link>
          <Link to="/visma-twitter" className="Tile">
            <div className="Tile-Title">#Visma</div>
            <img className="Tile-Icon" src={twitter} />
          </Link>
          <Link to="/visma-twitter" className="Tile">
            <div className="Tile-Title">#Visma</div>
            <img className="Tile-Icon" src={twitter} />
          </Link>
          <Link to="/visma-twitter" className="Tile">
            <div className="Tile-Title">#Visma</div>
            <img className="Tile-Icon" src={twitter} />
          </Link>
          <Link to="/visma-twitter" className="Tile">
            <div className="Tile-Title">#Visma</div>
            <img className="Tile-Icon" src={twitter} />
          </Link>
        </div>
      </div>
    );
  }
}
