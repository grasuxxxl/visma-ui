import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import logo from './logo.svg';
import NotAuthenticated from './components/NotAuthenticated';
import Authenticated from './components/Authenticated';
import Login from './components/Login';
import ForgotPassword from './components/ForgotPassword';
import RecoverPassword from './components/RecoverPassword';
import Launchpad from './components/Launchpad';
import VismaTwitter from './components/VismaTwitter';

import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { user: null }; // TODO: Change user to null
  }

  getChildContext() {
    return {
      user: this.state.user
    }
  }

  onLoginSuccess({ user }) {
    this.setState({
      user
    })
  }

  render() {
    return (
      <Router>
        <div className="App">
          <NotAuthenticated>
            <Route exact path="/" render={() => (<Login onLoginSuccess={this.onLoginSuccess.bind(this)} />)} />
            <Route exact path="/recover-password" component={RecoverPassword} />
            <Route exact path="/forgot-password" component={ForgotPassword} />
          </NotAuthenticated>

          <Authenticated>
            <Route exact path="/" component={Launchpad} />
            <Route path="/visma-twitter" component={VismaTwitter} />
          </Authenticated>

        </div>
      </Router>
    );
  }
}

App.childContextTypes = {
  user: PropTypes.object
};

export default App;
