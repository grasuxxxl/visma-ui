import React, { Component } from "react";

export default class Input extends Component {
  onFocus() {
    console.log("focus");
  }

  render() {
    const { name, label, onChange, value, type = "text" } = this.props;

    return (
      <div class="input">
        <input
          type={type}
          value={value}
          name={name}
          id={name}
          onChange={onChange}
          placeholder={label}
          onFocus={this.onFocus.bind(this)}
        />
        <span class="spin" />
      </div>
    );
  }
}
