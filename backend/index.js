const express = require('express');
const OAuth = require('oauth');
const app = express();



app.post('/api/authenticate', (req, res) => res.send(JSON.stringify({ user: { fullName: "Ionel Dita" }})));
app.post('/api/forgot', (req, res) => res.send("{}"));
app.post('/api/recover', (req, res) => {
  res.send("{}");
});


app.get('/api/tweets/search', (req, res) => {
  var oauth = new OAuth.OAuth(
    'https://api.twitter.com/oauth/request_token',
    'https://api.twitter.com/oauth/access_token',
    '1qP4G9jSCXNr3yFFCCOpUrpH2',
    '8R0Gi17rGBC85XTokHvGKjvQ1TAKGklCdqrKw7RYmf8teEYSdP',
    '1.0A',
    null,
    'HMAC-SHA1'
  );

  const url = req.query.maxId === undefined
    ? `https://api.twitter.com/1.1/search/tweets.json?q=${req.query.query}&result_type=popular`
    : `https://api.twitter.com/1.1/search/tweets.json?max_id=${req.query.maxId}&q=${req.query.query}&result_type=popular`;

  oauth.get(
    url,
    '88983206-FWy5kmZDL75iQrTNFCkfVDvQDCSjzSGg0OxwEGf7W', //test user token
    'tNZiw6PtpQfd4d3cuRMdt3gpE5kf89icVdZnsMmfZH7OY', //test user secret
    function (e, data){
      if (e) {
        console.error(e);
        res.send(JSON.stringify({ error: e }));
      } else {
        res.send(data);
      }

    });

});
app.listen(4000, () => console.log('Example app listening on port 3000!'));